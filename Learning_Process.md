# Learning Process

## 1. How to Learn Faster with the Feynman Technique
### What is the Feynman Technique?


This video mainly focuses on step by step technique on how to understand something in a better way. So in order to understand something better we must be able to explain it to someone in a better way that's what is called the Feynman Technique. It's the best way to understand all the nitty-gritty details of the concpets.

### What are the different ways to implement this technique in our learning process

* If we are shaky at some of  the concepts and we want to understand it better then we can use this method.
* It can also be effective when we have a test coming in soon and we want to test our understanding and challenge our assumptions.


## 2. Learning How to Learn TED talk by Barbara Oakley

### Key takeaways from the Video.
* We should keep on learning new things and add them to our repository.
* Everyone learn the same thing but it depends upon the person that how much he is able to understand it in a better way.
* To be able to concentrate more on we must take certain amount brake in between the learning process.
* In between the brakes we must not forget the aim of our learning process.
* Relaxation is also a important part of learning process.
* We must keep on testing ourselves to get into a conclusion on where we stand.

### What are some of the steps that you can take to improve your learning process?
* We must be able to explain in a better way to others.
* We must follow the focused and diffuse mode method.
* Once switched to focused mode we must be on that mode for certain period of time.
* Once switched to diffused mode we must get back to focused mode towards the goal as soon as possible.
* We must keep on testing ourselves to understand the topics better.



## 3. Learn Anything in 20 hours

### Key takeaways from the Video.
* The dedication of learning new things has to be there throughout.
* As we start something new , we may face difficulties but as we go ahead and understand it things     become easy.
* The more we spend time in practicing something more better we get on it.

### Steps that we should follow while approaching a new new topic.

* **Deconstruct our skills** - What will we understand when we finish the topics. Based upon that we must break down the topics into several sub-topics and try to understand each one of them.

* **Learn enough to self correct** - We must go through enough resources from where we can guess the past scenarios and the coming scenarios.

* **Remove practice barriers** - We must have stay away from the things that may cause distraction in our learning process like there has to be a proper understanding that why we are larning that topic.

* **Practice atleast 20 hours** - In todays scenario it's easy to feel frustrated and our mind is the one to fool us here. So to be away from this frustration barrier we must fool our mind in turn and do the commitment that i'm going to sit for 4 hours and i'm going do it today.