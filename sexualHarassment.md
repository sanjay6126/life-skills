# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

* repeated compliments of an employee's appearance
* commenting on the attractiveness of others in front of an employee
* discussing one's sex life in front of an employee
* asking an employee about his or her sex life
* circulating nude photos or photos of women in bikinis or shirtless men in the workplace
* making sexual jokes
* sending sexually suggestive text messages or emails
* leaving unwanted gifts of a sexual or romantic nature
* spreading sexual rumors about an employee, or
* repeated hugs or other unwanted touchings.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
* support a complaint made by the person who experienced the sexual harassment
* report what I've seen
* give evidence as a witness, for example at a hearing
* make a sexual harassment complaint because what I've seen has created an intimidating, hostile, humiliating or offensive environment.